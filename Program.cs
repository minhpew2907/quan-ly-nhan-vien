﻿
EmployeeManager employeeManager = new EmployeeManager();
int choice = 0;

do
{
    try
    {
        employeeManager.Menu();
        choice = int.Parse(Console.ReadLine());
        Console.Clear();
        switch (choice)
        {
            case 1:
                employeeManager.PrintEmployee();
                break;
            case 2:
                employeeManager.AddEmployee();
                break;
            case 3:
                employeeManager.RemoveEmployee();
                break;
            case 4:
                Console.WriteLine("Thank you, see you again");
                Environment.Exit(0);
                break;
            default:
                Console.WriteLine("Choose number on board");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 4);
