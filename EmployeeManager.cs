﻿


using EmployeeManagement;

class EmployeeManager
{
    Employee[] employees = new Employee[100];

    public void PrintEmployee()
    {
        foreach (var item in employees)
        {
            if (item != null)
            {
                Console.WriteLine($"Name: {item.Name}, Age: {item.Age}, Position: {item.Position}, Salary: {item.Salary}");
            }
        }
        Console.WriteLine("The information is empty");
    }

    public void AddEmployee()
    {
        if (employees.Length > 0)
        {
            Employee employee = new Employee();
            Console.WriteLine("Enter employee name: ");
            employee.Name = Console.ReadLine();
            Console.WriteLine("Enter employee age: ");
            employee.Age = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter employee position: ");
            employee.Position = Console.ReadLine();
            Console.WriteLine("Enter employee salary: ");
            employee.Salary = float.Parse(Console.ReadLine());

            Array.Resize(ref employees, employees.Length + 1);
            employees[employees.Length - 1] = employee;

            Console.WriteLine("Add successfully!");
        }
        else
        {
            Console.WriteLine("Can not add more employee, because list is full");
        }
    }

    public void RemoveEmployee()
    {
        Console.WriteLine("Enter name employee to remove: ");
        string employeeName = Console.ReadLine();

        for (int i = 0; i < employees.Length; i++)
        {
            if (employees[i] != null && employees[i].Name == employeeName)
            {
                employees[i] = null;
                Console.WriteLine($"Employee {employeeName} has been removed");
            }
        }
        Console.WriteLine("The list is empty");
    }

    public void Menu()
    {
        Console.WriteLine("----Employee manager-----");
        Console.WriteLine("1. View employee information");
        Console.WriteLine("2. Add new employee");
        Console.WriteLine("3. Remove employee");
        Console.WriteLine("4. Exit");
        Console.WriteLine("Enter your choose: ");
    }
}